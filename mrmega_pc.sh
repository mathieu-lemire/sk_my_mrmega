#!/bin/bash 


# file contains a list of mrmega input files (1st column) and names (2nd) 
inputfiles=$1
npc=$2 

files=`cat $inputfiles | awk '{print $1}'`
echo $files
nf=`echo $files | awk '{print NF}'`
names=`cat $inputfiles | awk '{print $2}'`
names=($names)

function leadingzero {
 echo "00000"$1  | rev | cut -c -2 | rev 
}

i=0
for f in $files; do 
 i=$[$i+1]
 kk=`leadingzero $i`
 echo $kk
 zcat $f | awk '{print $1}' > _${kk}_snps 
done 


# this is the list of snps common to all studies

cat _[0-9][0-9]_snps | sort | uniq -c | awk '$1=="'$nf'" {print $2}'   > _get_common

# extracting these SNPs 

i=0
for f in $files; do
 echo $f
 i=$[$i+1]
 kk=`leadingzero $i `
 if [ -f _${kk}.gz ]; then 
   \rm _${kk}.gz
 fi 
 zcat $f | head -1 > _${kk}
 zcat $f | tail -n +2 | sort > ${kk}_${names[$[$i-1]]}.txt  
 join -1 1 -2 1 _get_common ${kk}_${names[$[$i-1]]}.txt  >> _${kk}
 gzip _${kk}
done 
ls -1  _[0-9][0-9].gz > _mrmega.in

MR-MEGA --qt --filelist _mrmega.in --out mrmega2 --debug --pc $npc  > mrmega${npc}.out

\rm _foo 
\rm _get_common 







