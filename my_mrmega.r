
args<- commandArgs(TRUE)

data<- read.table(args[1], head=F )
pc<- read.table(args[2], head=F )
pc<- pc[,-1] 
names(pc)<- paste0( "x", 1:ncol(pc) )

output<- args[3]



#checking for allele order
a1<- data[ , seq( 4,ncol(data), 8 ) ]
tmp1<- apply( a1, 1, function(a){ length( unique( na.omit(a ) ) )}  )
a2<- data[ , seq( 5,ncol(data), 8 ) ]
tmp2<- apply( a2, 1, function(a){ length( unique( na.omit(a ) ) )}  )

sel<- tmp1!=1 | tmp2!=1

if( sum(sel) > 1 ){
 write.table( data[sel,] , paste0(output,"_ALLELE_PROBLEM" ), col=F, row=F, quote=F )
 data<- data[!sel,]
}


beta<- data[, seq(8,ncol(data),8)]
se<-  data[, seq(9,ncol(data),8)]
n <-  data[, seq(7,ncol(data),8)]

# total sample size 
ss<- apply( n , 1, sum, na.rm= T )
mx<- max( ss )
# number of studies 
ns<- apply( !is.na( n ) , 1, sum )

sel<- ss > .5*mx  & ns > 3

beta<- beta[sel,]
se<- se[sel,]
n<- n[sel,]
ss<-ss[sel]
ns<-ns[sel]
data<- data[sel,] 

data$ss<- ss
data$ns<- ns 

f<- function( i ){
 y<- as.numeric( t( beta[i,] ) )
 s<- as.numeric( t( se[i,] ) )
 w<- 1/as.numeric( s  )^2 
 d<- data.frame( y=y, pc , w=w )
 d<- d[ !apply( is.na(d), 1, any ), ] 
 form<- as.formula(paste(  "y ~", paste( names(pc), collapse="+")  )  )
 full<- lm( form, weights=d$w, data=d )
 null<- lm( y ~ 0, weights=d$w, data=d )
 chi<-  deviance(null) - deviance(full )
 p<-pchisq(  chi ,length( coef(full) ) , lower=F  ) 
 return(c( chi, p) )
}

tmp <-t(  apply( data.frame( 1:nrow( beta ) ), 1,  f ) )
chi<- tmp[,1]
pv<- tmp[,2] 

if( length( pv ) == nrow(data ) ){
 data$chi<- chi 
 data$pv<- pv 
 data$file<- args[1]
 write.table(data, output, row=F,col=F, quote=F )
} else {
 write.table( data, paste0(output,"_PROBLEM" ), col=F, row=F, quote=F )
}



# 9:98895004:G:T 







