
##############################################################################################
# v0.1.1 lambda now based on chi2 and not log10(p)
# v0.1.0 first
#############################################################################################

args<- commandArgs(TRUE)
# csv file with header CHROM,POS,PVALUE,ID
input<- args[1]
#
output.prefix <- args[2]

r<- read.csv(input) 
names( r )<-c("CHROM","POS","PVALUE","ID")

r<-r[ !is.na( r$PVALUE ), ] 



r$CHROM<-as.character(r$CHROM)
sel<- r$CHROM=="X"
r$CHROM[sel]<-"23"
r$CHROM<- as.numeric( r$CHROM )

mxchr<- max( r$CHROM )

mx<-rep(NA,mxchr) 
adjpos<-r$POS
for( chr in c(1:mxchr )  ){
cat( chr," " )
 if( any(  r$CHROM==chr ) ){ 
  mn<-min( r$POS[r$CHROM==chr ] )
  adjpos[ r$CHROM==chr ]<-adjpos[ r$CHROM==chr ] - mn + 1
  mx[chr]<-max( adjpos[ r$CHROM==chr ] ) + 10000000
 }
}

cmx<-cumsum(mx)
for( chr in c(2:mxchr)  ){
 if( any(  r$CHROM==chr  ) ){
  adjpos[ r$CHROM==chr ] <- adjpos[ r$CHROM==chr ] + cmx[ chr-1 ]
 }
}


at<-c(1,cmx+.5,max(adjpos) )
md<-(at[2:(mxchr+1)]-at[1:mxchr])/2+at[1:mxchr]




r$PVALUE[ r$PVALUE< 1e-10 ]<- 1e-10 



PCH<-rep( 19, nrow(r) )

CEX<-.25 
PCH<- 19 

sa<- 1:nrow(r) 
sa<- r$PVALUE<0.05 
MNP<- min(  -log10( r$PVALUE )[sa] , na.rm=T )

library( RColorBrewer )
colorpal<- c( brewer.pal( 9, "Set1" )[-6 ],  brewer.pal( 8, "Set2" ),  brewer.pal( 12, "Set3" )[-c(2,12) ] )
colorpal<-rep( c( gray(.25)  , gray(.75) ), 13 ) 

colorpal<-rep( c("#7497df", "#fcbd20")  , 13 ) 



x<-  adjpos[sa]
y<-  -log10( r$PVALUE )[sa]
CEX<- .5*( (y-min(y))/(max(y)-min(y))+.25  )


bitmap( paste0(output.prefix,"_manh.tiff"), type="tiff32nc",height=178/2 , 
  width=178   ,units="mm", res=300, 
  pointsize=10  )

# sampling 10% of the points if y<2 , 100% if y>2
sel <-sort(   unique(   c( 1, seq( 1, length(x), length=floor( length(x)/10 ) ), length(x) ,  which( y > 2 ) ) ) ) 

plot(x[sel] ,y[sel]  , pch = PCH , cex=CEX[sel]   , 
  col=colorpal[ r$CHROM[sa] ][sel]  , xaxt="n", yaxt="n", ylab="-log10(pv)"  ,  cex.lab=1 ,
  xlab="Chromosome/Position" , ylim=c(MNP, 10.25) )

# ylim=c(MNP,CEI<- ceiling(-log10(min( r$PVALUE )))   )  )

for( i in seq( 1,mxchr ,2 )  ){
axis( 1,at=md[i], label=i , cex.axis=1 ,padj=.25 + .25 , tcl=-1  )
}

for( i in seq( 2,mxchr,2 )  ){
axis( 1,at=md[i], label=i , cex.axis=1 , padj=-1 + .25  )
}
axis( 2, at=seq( 0,8,2 ), cex.axis=1   )

abline(h=-log10( 5e-8) , lty=3 )
dev.off()



bitmap( paste0(output.prefix,"_qq.tiff"), type="tiff32nc",height=178/2 , width=178/2   ,units="mm", res=300, 
  pointsize=10  )


pp<- sort( r$PVALUE, decreasing=TRUE) 
 x<- (-log10(1- ppoints( pp ) ) )  
 y<- (-log10( pp  )) 

 o<- order( y )
 x<- x[o]
 y<- y[o]

CEX<- .5*( (y-min(y))/(max(y)-min(y))+.25  )
CEX<-CEX[o] 

CEI<-10.25

qqplot( max(x) , max(y),  
        pch=19, cex=CEX , col="black" , xlim=c(0,CEI ), ylim=c(0,CEI ) , xlab="Expected quantiles", ylab="-log10(pv)", cex.axis=1, cex.lab=1   )


#lambda<- floor( 1000* median(  qchisq( 1-pp,1 )   ,na.rm=T)/qchisq(.5,1)   +.5 )/1000 
#legend( x="bottomright", legend=paste0("lambda=",lambda ), bty="n"  )



xx<-  -log10(  ppoints( n<- length( x )   ) )
sel<- sort( unique( c( 1:1000, 2^(0:ceiling( log(n,2) ) ) ) )  )
sel[ sel> n ]<- n 

qu<- -log10(qbeta( 0.975, sel , (n+1-sel) ))
ql<- -log10(qbeta( 0.025, sel , (n+1- sel) ))

points(xx[sel],qu ,type='l', col=gray(.75), lwd=3 )
points(xx[sel],ql ,type='l', col=gray(.75), lwd=3 )



sel<- sort( unique( c( 1:sum(pp<0.001, na.rm=T )  ,  seq( 1,n, length=floor( n/100 ) ) ) ) ) 
sel[ sel> n ]<- n

points( x[ n-sel+1] ,y[n-sel+1] , pch=19, cex=CEX[n-sel+1] , col="black" )

dev.off() 





